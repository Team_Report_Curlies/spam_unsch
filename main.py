from bayes_util import Nodo, NodoHijo, Distribucion

d = Nodo("ga", .4)
e = Nodo("jar", .8)
f = NodoHijo(d, e)

dist = Distribucion([0.2, .3])

print(dist.get_valor(0,0,0))
print(f.padres)
