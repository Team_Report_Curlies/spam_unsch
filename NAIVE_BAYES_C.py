import pandas as pd

def get_from_sigla(data, sigla) :
    return data[data.sigla == sigla]

def get_probabilidad_total(data, umbral) :
    lista_total = data.total.to_list()
    contador = 0
    for i in lista_total:
        if (i > umbral):
            contador += 1
    return contador/len(lista_total)

def get_prob_acierto_por_valor(data, valor, umbral) :
    sel = data[data.total > umbral]
    l = sel.tipo.to_list()
    contador = 0
    for i in l:
        if (i == valor):
            contador += 1
    return contador/len(l)

def get_prob_fracaso_por_valor(data, valor, umbral) :
    sel = data[data.total <= umbral]
    l = sel.tipo.to_list()
    contador = 0
    for i in l:
        if (i == valor):
            contador += 1
    return contador/len(l)

data = pd.read_csv("data.csv")


def get_pred(umbral = 30, sigla = 'AC-121'):  
    mod = get_from_sigla(data, sigla)
    
    print("Eventos :")
    print("A: mas de " , umbral , " alumnos se matriculen en el curso")
    print("B: mas de ", umbral,  " alumnos se matriculen en cierto tipo de matricula")
    
    prob_aciertos = get_probabilidad_total(mod, umbral)
    prob_fracaso = 1 - prob_aciertos
    
    print("P(A) =" , prob_aciertos)
    print("P(~A) = ", prob_fracaso)
    
    prob_aciertos_dado_reg = get_prob_acierto_por_valor(mod, 'REGULAR', umbral)
    prob_aciertos_dado_apla = get_prob_acierto_por_valor(mod, 'APLAZADOS', umbral) 
    prob_aciertos_dado_vaca = get_prob_acierto_por_valor(mod, 'VACACIONAL', umbral) 
    prob_aciertos_dado_susti = get_prob_acierto_por_valor(mod, 'SUSTITUTORIO', umbral) 
    
    print("P(A|B=REGULAR) = ", prob_aciertos_dado_reg)
    print("P(A|B=APLAZADOS) = ", prob_aciertos_dado_apla)
    print("P(A|B=VACACIONAL) = ", prob_aciertos_dado_vaca)
    print("P(A|B=SUSTITUTORIO) = ", prob_aciertos_dado_susti)
    
    prob_fracaso_dado_reg = get_prob_fracaso_por_valor(mod, 'REGULAR', umbral)
    prob_fracaso_dado_apla = get_prob_fracaso_por_valor(mod, 'APLAZADOS', umbral)
    prob_fracaso_dado_vaca = get_prob_fracaso_por_valor(mod, 'VACACIONAL', umbral)
    prob_fracaso_dado_susti = get_prob_fracaso_por_valor(mod, 'SUSTITUTORIO', umbral)
    
    print("P(~A|B=REGULAR) = ", prob_fracaso_dado_reg)
    print("P(~A|B=APLAZADOS) = ", prob_fracaso_dado_apla)
    print("P(~A|B=VACACIONAL) = ", prob_fracaso_dado_vaca)
    print("P(~A|B=SUSTITUTORIO) = ", prob_fracaso_dado_susti)
    
    #PROBABILIDAD TOTAL:
    prob_reg = prob_aciertos_dado_reg*prob_aciertos + prob_fracaso_dado_reg*prob_fracaso
    prob_apla = prob_aciertos_dado_apla*prob_aciertos + prob_fracaso_dado_apla*prob_fracaso
    prob_vaca = prob_aciertos_dado_vaca*prob_aciertos + prob_fracaso_dado_vaca*prob_fracaso
    prob_susti = prob_aciertos_dado_susti*prob_aciertos + prob_fracaso_dado_susti*prob_fracaso
    
    print("P(B=REGULAR) = ", prob_reg)
    print("P(B=APLAZADOS) = ", prob_apla)
    print("P(B=VACIONAL) = ", prob_vaca)
    print("P(B=SUSTITUTORIO) = ", prob_susti)
    
    #PROB CONDICIONAL
    prob_reg_dado_aciertos = 0
    prob_reg_dado_fracaso = 0
    if (prob_reg != 0):
        prob_reg_dado_aciertos = prob_aciertos_dado_reg*prob_reg/prob_aciertos
        prob_reg_dado_fracaso = prob_fracaso_dado_reg*prob_reg/prob_fracaso
    if (prob_aciertos_dado_reg == 1):
        prob_reg_dado_aciertos = 1.0
    if (prob_fracaso_dado_reg == 1):
        prob_reg_dado_fracaso = 1.0
        
    prob_apla_dado_aciertos = 0
    prob_apla_dado_fracaso = 0
    if (prob_apla != 0):
        prob_apla_dado_aciertos = prob_aciertos_dado_apla*prob_apla/prob_aciertos
        prob_apla_dado_fracaso = prob_fracaso_dado_apla*prob_apla/prob_fracaso
    if (prob_aciertos_dado_apla == 1):
        prob_apla_dado_aciertos = 1.0
    if (prob_fracaso_dado_apla == 1):
        prob_apla_dado_fracaso = 1.0
    
    prob_vaca_dado_aciertos  = 0
    prob_vaca_dado_fracaso = 0
    if (prob_vaca != 0) :
        prob_vaca_dado_aciertos = prob_aciertos_dado_vaca*prob_vaca/prob_aciertos
        prob_vaca_dado_fracaso = prob_fracaso_dado_vaca*prob_vaca/prob_fracaso
    if (prob_aciertos_dado_vaca == 1):
        prob_vaca_dado_aciertos = 1.0
    if (prob_fracaso_dado_vaca == 1):
        prob_vaca_dado_fracaso = 1.0
    
    prob_susti_dado_fracaso = 0
    prob_susti_dado_aciertos = 0
    if (prob_susti != 0):
        prob_susti_dado_aciertos = prob_aciertos_dado_susti*prob_susti/prob_aciertos
        prob_susti_dado_fracaso = prob_fracaso_dado_susti*prob_susti/prob_fracaso
    if (prob_aciertos_dado_susti == 1):
        prob_susti_dado_aciertos = 1.0
    if (prob_fracaso_dado_susti == 1):
        prob_susti_dado_fracaso = 1.0
    
    
    print()
    print('RESULTADOS: ')
    
    print('P(B=REGULAR|A) = ', prob_reg_dado_aciertos)
    print('P(B=APLAZADOS|A) = ', prob_apla_dado_aciertos)
    print('P(B=VACIONAL|A) = ', prob_vaca_dado_aciertos)
    print('P(B=SUSTITUTORIO|A)', prob_susti_dado_aciertos)
    
    print('P(B=REGULAR|~A) = ', prob_reg_dado_fracaso)
    print('P(B=APLAZADOS|~A) = ', prob_apla_dado_fracaso)
    print('P(B=VACIONAL|~A) = ', prob_vaca_dado_fracaso)
    print('P(B=SUSTITUTORIO|~A)', prob_susti_dado_fracaso)



get_pred(50, 'IS-348')