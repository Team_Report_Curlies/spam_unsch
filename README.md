﻿# SPAM_UNSCH

SPAM: Sistema de Predicción de Alumnos Matriculados
Sistemas Expertos - Ingeniería de Sistemas UNSCH

## Requisitos
```bashd
anaconda, Librearias: NumPy, Pandas, SKlearn, Naive Bayes
```

## Instalación

Clona este repositorio

```bash
git clone git@bitbucket.org:Team_Report_Curlies/spam_unsch.git
```
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate..

## License
[MIT](https://choosealicense.com/licenses/mit/)