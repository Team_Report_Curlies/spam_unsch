class ErrorEstadistico(BaseException):
    
    def __init__(self, message):
        BaseException(message)

class Nodo:
    
    def __init__(self, 
                 nombre,
                 valor):
        
        self.nombre = nombre
        
        if (valor <= 1.0):
            self.valor_0 = valor
            self.valor_1 = 1.0 - valor
        
        else:
            
            raise ErrorEstadistico("El valor introducido debe ser menor o igual a 1.0")
    
    def set_nombre(self, nombre):
        self.nombre = nombre
    
    

class NodoHijo:
    
    def __init__(self, *padres):
        
        self.padres = padres


class Distribucion3Var:
    
    def __init__(self, valores):
        
        for i in valores:
            if (i <= 1.0 and i >= 0.0):
                raise ErrorEstadistico("Los valores intorducidos deben ser mayor de 0.0 o menor 1.0")
        self.valores = valores
        
    def get_valor(self, i, j, k):
        
        return self.valores[int(str(i) + str(j) + str(k), 2)]
    
class Distribucion2Var():
    
    def __init__(self, valores):
        
        for i in valores:
            if (i <= 1.0 and i >= 0.0):
                raise ErrorEstadistico("Los valores intorducidos deben ser mayor de 0.0 o menor 1.0")
        self.valores = valores

    def get_valor(self, i, j):
        
        return self.valores[int(str(i) + str(j), 2)]

def get_bayes(a_b, a):
    return a_b/a
