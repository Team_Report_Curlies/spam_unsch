class Curso:
    """
    Representa un curso dentro de SPAM.
    Guarda atributos como sigla, nombre, requisito, secuencia
    """
    
    def __init__(self, 
                 sigla="", 
                 nombre="", 
                 requisito="", 
                 secuencia=""):
        
        self.sigla = sigla
        self.nombre = nombre
        self.requisito = requisito
        self.secuencia = secuencia
    
    def __iter__(self):
        yield 'sigla'
        yield 'nombre'
        yield 'requisito'
        yield 'secuencia'
        
        
class Evento:
    """
    Representa un evento en SPAM, es decir,
    aplazados, regular, vacacional, curso unico
    """
    
    def __init__(self,
                 sigla = "",
                 aprobados = 0,
                 tipo = "",
                 semestre = ""):
        
        self.sigla = sigla
        self.aprobados = aprobados
        self.tipo = tipo
        self.semestre = semestre
    
    def __iter__(self):
        yield 'sigla'
        yield 'aprobados'
        yield 'tipo'
        yield 'semestre'
        
class Matriculados:
    """
    Representa el total de alumnos matriculados.
    Guarda los atributos: sigla, total, semestre
    """
    
    def __init__(self,
                 sigla = "",
                 total = 0,
                 tipo = "",
                 semestre = ""):
        
        self.sigla = sigla
        self.total = total
        self.tipo = tipo
        self.semestre = semestre
    
    def __iter__(self):
        yield 'sigla'
        yield 'total'
        yield 'tipo'
        yield 'semestre'

class Desmatriculados:
    """
    Representa alumnos desmatriculados.
    Guarda los atributos: sigla, total, semestre
    """
    
    def __init__(self,
                 sigla = "",
                 total = 0,
                 semestre = ""):
        
        self.sigla = sigla
        self.total = total
        self.semestre = semestre
    
    def __iter__(self):
        yield 'sigla'
        yield 'total'
        yield 'semestre'