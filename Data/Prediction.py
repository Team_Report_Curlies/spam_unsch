# -*- coding: utf-8 -*-
"""

@author: Jmab
"""

import pandas as pd
import numpy as np

df_train = pd.read_csv("Datos_SPAM_train2.csv")
df_test = pd.read_csv("Datos_SPAM_test2.csv")



print("Data Frame")
print("Datos de Entrenamiento")
print("----"*20)
print(df_train)
print("Datos de Test")
print("----"*20)
print(df_test)

print("----"*20)
print("Cantidad de DAtos")
print(df_train.shape)
print(df_test.shape)

print("----"*20)
print("Tipos de Datos")
print(df_train.info())
print("----"*20)
print(df_test.info())


print("----"*20)
print("Descripción del dataset")
print(df_train.describe())
print(df_train.describe(include="all"))
print(df_test.describe())
print(df_test.describe(include="all"))

print("----"*20)
print("Distribución de los cursos")
""" Cant de datos x sigla"""
print(df_train.groupby('nombre').size())


from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB

##Construcción del modelo
#Separo todos los datos con las características y las etiquetas y resultados

df_train = df_train.drop(['nombre'],axis=1)

print("----"*20)
print("Forma de nuestros Data set")
print(df_train.shape)
print(df_test.shape)



X = np.array(df_train.drop(['PMP'],1))
y = np.array(df_train['PMP'])

#Separo los datos de "train" en entrenamiento y prueba para probar algoritmos
X_train, X_test,y_train, y_test = train_test_split(X,y, test_size=0.2)

print("----"*20)
print("Son {} datos para entrenamiento y {} datos para prueba".format(X_train.shape[0],X_test.shape[0]))
print("----"*20)

#Aplicacion de los algoritmos de machine learning
#Modelo de regresion Logistica
logreg = LogisticRegression()
logreg.fit(X_train,y_train)
Y_pred = logreg.predict(X_test)
print("Precision Regresion Logistico: {} ".format(logreg.score(X_train,y_train)))
print("----"*20)

#Modelo de maquinas de vectores de soporte
svc = SVC()
svc.fit(X_train,y_train)
Y_pred = svc.predict(X_test)
print("Precisión de Maquinas de Vectores de Soporte: {}".format(svc.score(X_train,y_train)))
print("----"*20)

#Modelo de Vecinos cercanos
knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train,y_train)
Y_pred = knn.predict(X_test)
print("Precisión de Vecinos Cercanos: {}".format(knn.score(X_train,y_train)))
print("----"*20)

#Modelo de Arboles de DEcision Clasificacion
ad = DecisionTreeClassifier()
ad.fit(X_train,y_train)
Y_pred = ad.predict(X_test)
print("Precisión Arboles de Decision Clasificacion: {}".format(ad.score(X_train,y_train)))
print("----"*20)

gnb = GaussianNB()
gnb.fit(X_train,y_train)
Prediction= gnb.predict(X_test)
print("Precision de Naive Bayes: {}".format(gnb.score(X_train,y_train)))

### Realizamos la prediccion 
ids = df_test['nombre']

##REgresion logistica
prediccion_logreg = logreg.predict(df_test.drop('nombre',axis=1))
out_logreg = pd.DataFrame({'nombre':ids,'PMP': prediccion_logreg})
print('Prediccion Regresion Logistica: ')
print(out_logreg)
print("----"*20)

##Vectores de Apoyo
prediccion_svc = svc.predict(df_test.drop('nombre',axis=1))
out_svc = pd.DataFrame({'nombre':ids,'PMP': prediccion_svc})
print('Prediccion Vectores de Apoyo: ')
print(out_svc)
print("----"*20)

##VVecinos cercanos
prediccion_knn = knn.predict(df_test.drop('nombre',axis=1))
out_knn = pd.DataFrame({'nombre':ids,'PMP': prediccion_knn})
print('Prediccion Vecinos CErcanos: ')
print(out_knn)
print("----"*20)

##Arboles
prediccion_ad = ad.predict(df_test.drop('nombre',axis=1))
out_ad = pd.DataFrame({'nombre':ids,'PMP': prediccion_ad})
print('Prediccion Arboles de decision: ')
print(out_ad)
print("----"*20)

##Predicción con Naive Bayes
Prediction= gnb.predict(df_test.drop('nombre',axis=1))
out_gnb = pd.DataFrame({'nombre':ids,'PMP': Prediction})
print('Prediccion Naive Bayes: ')
print(out_knn)
print("----"*20)

