import pandas as pd
import numpy as np

df = pd.read_csv("Datos_SPAM.csv")



df.head()

df.tail()

print("Data Frame")
print(df)


print("Datos del DataFrame")
print(df.dtypes)


print("Descripción del dataset")
print(df.describe())
print(df.describe(include="all"))

print("Información del dataset")
print(df.info())





print("Distribución de los cursos")
""" Cant de datos x sigla"""
print(df.groupby('nombre').size())


from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier

##Construcción del modelo
#Separo todos los datos con las características y las etiquetas y resultados
#Z = np.array(df[2:7])
#print("Z",Z)
X = np.array(df.drop(['PMP','nombre','año'],1))
print(X)

y = np.array(df['PMP'])

#Separo los datos de "train" en entrenamiento y prueba para probar algoritmos
X_train, X_test,y_train, y_test = train_test_split(X,y, test_size=0.3)


print("Son {} datos para entrenamiento y {} datos para prueba".format(X_train.shape[0],X_test.shape[0]))

#Aplicacion de los algoritmos de machine learning
#Modelo de regresion Logistica
algoritmo = LogisticRegression()
algoritmo.fit(X_train,y_train)
Y_pred = algoritmo.predict(X_test)
print("Precision Regresion Logistico{} ".format(algoritmo.score(X_train,y_train)))

#Modelo de maquinas de vectores de soporte
algoritmo = SVC()
algoritmo.fit(X_train,y_train)
Y_pred = algoritmo.predict(X_test)
print("Precisión de Maquinas de Vectores de Soporte: {}".format(algoritmo.score(X_train,y_train)))


#Modelo de Vecinos cercanos
algoritmo = KNeighborsClassifier(n_neighbors=5)
algoritmo.fit(X_train,y_train)
Y_pred = algoritmo.predict(X_test)
print("Precisión de Vecinos Cercanos: {}".format(algoritmo.score(X_train,y_train)))

#Modelo de Arboles de DEcision Clasificacion
algoritmo = DecisionTreeClassifier()
algoritmo.fit(X_train,y_train)
Y_pred = algoritmo.predict(X_test)
print("Precisión Arboles de Decision Clasificacion: {}".format(algoritmo.score(X_train,y_train)))


### Realizamos la prediccion 












