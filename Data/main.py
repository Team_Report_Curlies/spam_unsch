# -*- coding: utf-8 -*-
"""
@author: Jmab
"""
"""
IMPORTAMOS LAS LIBRERÍAS Y PAQUETES A UTILIZAR
- Matplotlib.pyplot es el paquete para graficar funciones
- pandas es el paquete para analisis y manipulación de datos derivada de numpy
- tkinter para interfaz gráfica de usuario
- numpy: Extensión de python para operaciones de vectores, matrices con matematica de alto nivel
- time: Módulo de python para trabajar con fechas, horas, segundos, etc.
- webbrowser: Libreria que permite abrir archivos o sitios web desde python.
"""
import matplotlib.pyplot as plt
import pandas as pd
from tkinter import messagebox
import webbrowser as wb
import numpy as np
from tkinter import *
import time

"""
CREAMOS LA INTERFAZ GRÁFICA CON SUS CONTENEDORES
- config() Permite cambiar las características o configuraciones de un elemento usado en las interares gráficas.
- pack() "Empaqueta" el elemento dentro de la ventana.
- grid() Gestor geométrico, sirve para ordenar los elementos por filas y columnas.
"""
#Creamos la ventana principal con el método Tk() de Tkinter
root = Tk()
root.title("Predicción de Matriculados") #Asignamos un título a la ventana
root.iconbitmap("steam.ico") #Asignamos un icono
root.config(bg="white")  #Cambiamos el color de fondo.

#Creamos un contenedor o marco Nº 1 para nuestras etiquetas y cajas de texto.
frame = Frame(root, width = 600, height = 300)
frame.pack()
frame.config(bg = "white")

#Creamos un contenedor Nº 2 para los botones y el campo de texto con los datos.
frame2 = Frame(root,width = 600, height = 100)
frame2.pack()
frame2.config(bg = "white")

#Creamos el campo de texto para mostrar los datos
t1 = Text(frame2)
t1.grid(row = 1,column = 0, padx = 10, pady = 5, columnspan = 5)
t1.config(bg = "white")

"""
DEFINIMOS LAS FUNCIONES U OPERACIONES - CÁLCULO MATEMÁTICOS PARA EL PROGRAMA.
 --- Usaremos distintas librerias importadas previamente en los algoritmos de cada función.
"""
#leer_archivo(): lee las columnas y filas de un archivo excel y con la libreria pandas, introduce estos datos en un dataframe o matriz de datos.
def leer_archivo():
    print("Leyendo Archivo...")
    archivo = pd.read_csv('Datos_SPAM_test.csv')
    columnas = ['nombre', 'año', 'AM', 'AA','RM','RA','VM','VA']
    datos = archivo[columnas]
    return datos

#Actualiza el campo de texto con los datos del dataframe.
# delete borra los datos. delete(indice de inicio,indice de fin).
# insert ingresa los datos. insert(indice de inicio, indice de fin).
def actualizarCampo(datos, t1):
    t1.delete('1.0', END)
    t1.insert('end',datos)
    print("CAMPO ACTUALIZADO!")


#Función que actualiza los valores por mes según se seleccione éste.
#*args Se utiliza para pasar una lista de argumentos de longitud variable sin palabras clave. Ésto permite reconocer los cambios realizados
#por la función trace más adelante declarada.
#global nos permite trabajar con una variable en el programa. Es una variable global.
def cambios(*args):
    print("Cambio Realizado")
    print(var.get())
    global datosE,indice,a
    calculosFinales(datosE,var.get(),indice,a)

"""
DEFINIMOS FUNCIONES U OPERACIONES PARA GESTIONAR LA INTERFAZ Y SUS CAMBIOS.
"""
#INICIAMOS EL PROCESO DE CÁLCULO
#time.sleep(tiempo) = Pausa  o "duerme" la ejecución durante una cantidad de segundos.
#messagebox genera cuadros de dialogo con el usuario.
# showinfo(titulo del cuadro, mensaje) - Genera un cuadro de dialogo de aviso.
def iniciarProceso():
    global datosE
    datosE = hallarTempMedia(datosE)
    time.sleep(1.5)
    actualizarCampo(datosE,t1)
    messagebox.showinfo("Temperatura Media", "Se ha calculado la Temperatura Media")
    time.sleep(1.5)
    datosE = indiceClimatico(datosE)
    actualizarCampo(datosE,t1)
    messagebox.showinfo("Indice CLimático", "Se ha calculado el Indice Climático")
    global indice
    indice = indiceEfectivoAnual(datosE)
    ma.set(indice)
    time.sleep(1.5)
    messagebox.showinfo("Indice Efectivo Anual", "Se ha calculado  Indice Efectivo Anual = "+str(indice))
    global a
    a = exponente_a(indice)
    time.sleep(1.5)
    aa.set(a)
    messagebox.showinfo("Exponente a", "Se ha calculado el Exponente a = "+str(a))
    time.sleep(0.5)
    messagebox.showwarning("AVISO", "Para continuar con el proceso, seleccione el mes en el cual se hallará la Evapotranspiración.")

#Actualiza los campos calculados posteriores a la elección de un mes.
#Campos dependientes del mes.
def calculosFinales(datos,mes,indice,a):
    Evss = evapotranspiracionSC(datos, mes, indice, a)
    mr.set(Evss)
    art = factorCorreccion(datos, mes)
    fac.set(fact)
    evcc = evapotranspiracionC(datos, mes, indice, a)
    evc.set(evcc)
    messagebox.showinfo("Evapotranspiración", "La Evapotranspiración en el mes: "+mes+" es = "+str(evcc))
    time.sleep(1)
    messagebox.showinfo("AVISO", "Cálculos Finalizados")

#Abre el archivo excel con los datos usados en el programa.
#open_new(archivo) abre el archivo en una nueva ventana/pestaña
def editarDatos():
    wb.open_new('Datos_Senahmi_Mensual.xlsx')

"""
DEFINIMOS LAS FUNCIONES U OPERACIONES QUE GRAFICARAN SEGUN LOS DATOS.
    ---- Usaremos la libreria anteriormente importada.
- plot(x,y,tipo de punto y linea)
- xlabel(etiqueta,caracteristicas)
- tittle(titulo, características)
- show() muestra el gráfico.
"""

def graficoTemp():
    global datosE
    plt.plot(datosE['Mes'].values, datosE['T Max'].values,"D-",datosE['Mes'].values, datosE['T Min'].values,"d-",datosE['Mes'].values, datosE['T Med'].values,"*-")
    plt.xlabel("Mes", fontsize=15)
    plt.ylabel("Grados ºC", fontsize=15, color='blue')
    plt.title("TEMPERATURA", fontsize=20)
    plt.legend(('Tº Máxima', 'Tº Mínima', 'Tº Media'), loc='upper right')
    plt.show()

def graficoIndC():
    global datosE
    plt.plot(datosE['Mes'].values, datosE['Ind Clim'].values,"D-")
    plt.xlabel("Mes", fontsize=15)
    plt.ylabel("Indice Climático", fontsize=15, color='blue')
    plt.title("Indice Climático", fontsize=20)
    #plt.legend(('Tº Máxima'), loc='upper right')
    plt.ylim(0,10)
    plt.show()

def graficoHS():
    global datosE
    plt.plot(datosE['Mes'].values, datosE['Horas Sol'].values,"*-")
    plt.xlabel("Mes", fontsize=15)
    plt.ylabel("Horas de Sol", fontsize=15, color='red')
    plt.title("Horas de Sol por Mes", fontsize=20)
    #plt.legend(('Tº Máxima'), loc='upper right')
    plt.ylim(5,15)
    plt.show()

def graficoG():
    global datosE
    plt.plot(datosE['Mes'].values, datosE['T Med'].values,"D-",datosE['Mes'].values, datosE['Horas Sol'].values,"d-",datosE['Mes'].values, datosE['Ind Clim'].values,"*-")
    plt.xlabel("Mes", fontsize=15)
    plt.ylabel("Valor", fontsize=15, color='blue')
    plt.title("Gráfico General", fontsize=20)
    plt.legend(('Tº Media', 'Horas de Sol', 'Indice Climático'), loc='upper right')
    plt.show()

"""
DECLARAMOS LAS VARIABLES QUE USAREMOS EN EL PROGRAMA
"""
#variable que recibe el mes elegido.
var = StringVar()
var.set("Elija Mes")

#ma = indice efectivo anual. Esta variable almacena el valor del indice efectivo anual.
ma = StringVar()
#aa = valor del exponente a
aa = StringVar()
#evapotranspiracion sin corregir
mr = StringVar()
#factor de correcion
ar = StringVar()
#evapotranspiracion corregida
mv = StringVar()
av = StringVar()

pmp = StringVar()

#datosE recibe los valores del excel y los almacena en una matriz de datos - Es de tipo dataframe.
datosE = leer_archivo()
#actualizamos los campos.
actualizarCampo(datosE,t1)
#Inicializamos los valores de indice y el exponente a
indice = 0
a = 0
#El método trace() se emplea para "detectar" cuando una variable es leída, cambia de valor o es borrada
#"w" : ejecutar cuando alguien modifica la variable
#trace(tipo, funciona a ejecutar).
var.trace("w", cambios)

"""
CREAMOS EL CONTENIDO DE LA INTERFAZ GRÁFICA, ASIGNAMOS FUNCIONES, CONFIGURACIONES,ETC
- Label: Etiqueta de texto
- Entry: Caja de texto
- OptionMenu: Lista desplegable - Lista de Meses.
- Button: Boton

*sticky: ubica los elementos derecha (e) o izquierda (w).
*padx y pady: el tamaño del borde de cada elemento.
*columnspan: Cantidad de columnas que ocupa un elemento.
*textvariable: Es la variable que almacena el valor del elemento.
*command: Es la función que ejecuta el elemento.
"""

Label(frame,text= "Predicción de Alumnos Matriculados",bg = "white",font = "Helvetica 16 bold").grid(row = 0, column= 0,sticky = "w",padx=10,pady = 5, columnspan = 2)
Label(frame, text="Matriculados Aplazados",bg = "white").grid(row = 2, column= 0,padx=5,pady = 5, sticky = 'w')
Entry(frame, width=40, fg = "blue", justify = "center", textvariable = ma).grid(padx=5,pady = 5, row=2, column=1, sticky = 'w')
Label(frame, text="Aprobados Aplazados",bg = "white").grid(row = 3, column= 0,padx=5,pady = 5, sticky = 'w')
Entry(frame, width=40, fg = "blue", justify = "center", textvariable = aa).grid(padx=5,pady = 5, row=3, column=1, sticky = 'w')
Label(frame, text="Matriculados Regular",bg = "white").grid(row = 5, column= 0,padx=5,pady = 5, sticky = 'w')
Entry(frame, width=40, fg = "blue", justify = "center", textvariable = mr).grid(padx=5,pady = 5, row=5, column=1, sticky = 'w')
Label(frame, text="Aprobados Regular",bg = "white").grid(row = 6, column= 0,padx=5,pady = 5, sticky = 'w')
Entry(frame, width=40, fg = "blue", justify = "center", textvariable = ar).grid(padx=5,pady = 5, row=6, column=1, sticky = 'w')
Label(frame, text="Matriculados Vacacional",bg = "white",font = "Arial 10 bold").grid(row = 7, column= 0,padx=5,pady = 5, sticky = 'w')
Entry(frame, width=40, fg = "blue", justify = "center", textvariable = mv).grid(padx=5,pady = 5, row=7, column=1, sticky = 'w')
Label(frame, text="Aprobados Vacacional",bg = "white",font = "Arial 10 bold").grid(row = 7, column= 0,padx=5,pady = 5, sticky = 'w')
Entry(frame, width=40, fg = "blue", justify = "center", textvariable = av).grid(padx=5,pady = 5, row=7, column=1, sticky = 'w')
Label(frame, text="Promedio Matriculados Posible",bg = "white",font = "Arial 10 bold").grid(row = 7, column= 0,padx=5,pady = 5, sticky = 'w')
Entry(frame, width=40, fg = "blue", justify = "center", textvariable = pmp).grid(padx=5,pady = 5, row=7, column=1, sticky = 'w')

Label(frame, text="Curso: ",bg = "white",font = "Arial 9 bold", fg = "Red").grid(row = 4, column= 0,padx=5,pady = 5, sticky = 'w')
opciones = OptionMenu(frame, var, "Enero", "Febrero", "Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre")
opciones.config(width=40, bg = "white", fg = "red")
opciones.grid(row=4, column=1, sticky = 'w',padx=5,pady = 5)
Button(frame, text="Iniciar", command = iniciarProceso, bg = "white", fg = "blue", width = 30).grid(row = 1,column = 0, padx = 10, pady = 5, columnspan = 2)
Button(frame2, text="Editar Datos", command = editarDatos, bg = "white", fg = "blue", width = 12).grid(row = 0,column = 0, padx = 10, pady = 5,sticky = 'W')
Button(frame2, text="Gráfico Tº", command = graficoTemp, bg = "white", fg = "red", width = 12).grid(row = 0,column = 2, padx = 10, pady = 5,sticky = 'W')
Button(frame2, text="Gráfico Ind Clim", command = graficoIndC, bg = "white", fg = "red", width = 14).grid(row = 0,column = 3, padx = 10, pady = 5,sticky = 'W')
Button(frame2, text="Gráfico Horas de Sol", command = graficoHS, bg = "white", fg = "blue", width = 18).grid(row = 0,column = 1, padx = 10, pady = 5,sticky = 'W')
Button(frame2, text="Gráfico General", command = graficoG, bg = "white", fg = "red", width = 14).grid(row = 0,column = 4, padx = 10, pady = 5,sticky = 'W')

#Mantenems en ejecucion la ventana principal root.
root.mainloop()
