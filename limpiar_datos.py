from lector_datos import LectorDatos
from modelo import Curso, Evento, Matriculados, Desmatriculados
from conector import Conector


def insertar_todos_curso(lector_data, db_conn):
    
    for i in range(1, lector_data.num_filas()):
        
        db_conn.agregar(
                Curso(lector_data.leer_celda(i, 0), 
                      lector_data.leer_celda(i, 1),
                      '',
                      '')
                )
    
def insertar_todos_evento(lector_data, db_conn):
    
    for i in range(1, lector_data.num_filas()):
        
        db_conn.agregar(
                Evento(
                        lector_data.leer_celda(i, 0),
                        lector_data.leer_celda(i, 3),
                        lector_data.leer_celda(i, 4),
                        lector_data.leer_celda(i, 5)
                    )
            )
    
def insertar_todos_matriculados(lector_data, db_conn):
    
    for i in range(1, lector_data.num_filas()):
        
        fila = lector_data.leer_fila(i)
        db_conn.agregar(
                Matriculados(
                        fila[5],
                        fila[0],
                        fila[3],
                        fila[2]
                    )
            )
        

def insertar_todos_desmatriculados(lector_data, db_conn):
    
    for i in range(1, lector_data.num_filas()):
        db_conn.agregar(
                Desmatriculados(
                        lector_data.leer_celda(i, 0),
                        lector_data.leer_celda(i, 3),
                        lector_data.leer_celda(i, 5)
                    )
            )


conn = Conector('data/SPAM.sqlito')
#lec = LectorDatos('data/data.xlsx')

#insertar_todos_evento(lec, conn)
#insertar_todos_desmatriculados(lec, conn)

lec = LectorDatos('data/2015.xlsx')
insertar_todos_matriculados(lec,conn)
lec = LectorDatos('data/2016.xlsx')
insertar_todos_matriculados(lec,conn)
lec = LectorDatos('data/2017.xlsx')
insertar_todos_matriculados(lec,conn)
lec = LectorDatos('data/2018.xlsx')
insertar_todos_matriculados(lec,conn)
lec = LectorDatos('data/2019.xlsx')
insertar_todos_matriculados(lec,conn)