import xlrd as xl

class LectorDatos:
    def __init__(self, file):
        self.archivo_data = xl.open_workbook(file)
        self.hoja_calc = self.archivo_data.sheet_by_index(0)
        self.nombre_libro = self.hoja_calc.name
        
    def num_colum(self):
        return self.hoja_calc.ncols
    
    def num_filas(self):
        return self.hoja_calc.nrows
    
    def seleccionar_libro(self, indice_libro):
        self.hoja_calc = self.archivo_data.sheet_by_index(indice_libro)
        
    def leer_celda(self, num_fila, num_col):
        return self.hoja_calc.cell_value(num_fila, num_col)
    
    def toString(self):
        return str(self.hoja_calc.book) + ' ' + str(self.hoja_calc.nrows) + ' cols: ' + str(self.hoja_calc.ncols)