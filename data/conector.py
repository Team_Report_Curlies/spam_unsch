from sqlite3 import Error
import sqlite3

class Conector():
    """
    Es una interfaz de la base datos.
    """
    
    def __init__(self, ruta_db):
        """
        Parametros:
            ruta_db -- ruta hacia la base de datos
        """
        
        self.con = sqlite3.connect(ruta_db)
        self.cur = self.con.cursor()
    
    def optimizar_db(self):
        self.con.execute("VACUUM;")
        self.con.commit()
    
    def agregar(self, obj):
        """
        Inserta un objeto en la db
        """
        
        #Armar sql de inserción
        sql = """INSERT INTO """ + type(obj).__name__.lower() + "("
        for i in iter(obj):
            sql += i + ", "
        sql = sql.rstrip(', ')
        
        sql += ") VALUES("
        
        for i in iter(obj):
            sql += "?, "
        sql = sql.rstrip(', ')
        sql += ")"
            
        #obtener atributos
        data = list([obj.__dict__[i] for i in obj.__dict__])
        
        
        try:
            self.cur.execute(sql, tuple(data))
            
            if self.cur.lastrowid:
                self.con.commit()
            else:
                self.con.rollback()
        except Error as e:
            
            print('Oops : ', e)
        
        
        return id
    
    def actualizar(self, obj, criterio):
        sql = 'UPDATE ' + type(obj).__name__.lower() + ' SET '
                
        for atrib in iter(obj):
            sql += atrib + ' = ?, '
            
        sql = sql.rstrip(', ')
        
        for valor in list([obj.__dict__[i] for i in obj.__dict__]):
            if type(valor).__name__ == 'str':
                valor = "'" + valor +"'"
                
            sql = sql.replace("?", str(valor), 1)
        
        sql += ' WHERE ' + criterio
        
        try:
            self.cur.execute(sql)
            self.con.commit()
            
        except Error as e:
            
            print('Oops : ', e)
        
    
    def eliminar(self, obj):
        pass
    
    def obtener_por_id(self, obj, id):
        return self.obtener_por_criterio(obj, "rowid = " + str(id))
    
    def obtener_por_criterio(self, obj, criterio):
        """
        Realiza un aconsulta en la base de datos
        Parametros:
            obj -- el objeto a consultar
            criterio -- es el criterio de busqueda, es decir, WHERE <criterio>
        """
        sql = 'SELECT * FROM ' +  type(obj).__name__.lower()
        sql += ' WHERE ' + criterio
        
        print("QUERY: ", sql)
        
        try:
            self.cur.execute(sql)
        
        except Error:
            #print('Not matches found!')
            pass
        return self.cur.fetchall()
    
    def terminar_sesion(self):
        self.cur.close()
        self.con.close()